import jenkins
import logging
from telegram import Update
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext

from config import AUTH_CONFIG
from index import main
from index import run_query
from os import remove

#Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)

def start(update: Update, context):
    user = update.effective_user
    update.message.reply_text(f""" Hi! {user.username}: {user.first_name}
                                   /\_/\  (
                                  ( ^.^ ) _)
                                    \\"/  (
                                   ( || )
                                (__d b__)
    """)

def sync(update: Update, context):
    user = update.effective_user
    if not update.effective_chat.type == 'private':
        try:
            server = jenkins.Jenkins(url=AUTH_CONFIG['JK_URL'],username=AUTH_CONFIG['JK_USER'], password= AUTH_CONFIG['JK_PASS'])
            server.build_job('sync-s3-on-premise-carga')
            update.message.reply_text("Sincronizando, espere entre 20 a 25 segundos...🐈‍⬛")
            logger.info(f"El usuario: {user.username}: {user.first_name} hizo sync")
        except Exception as e:
            update.message.reply_text("Ha ocurrido un error al sincronizar")
            logger.error(e)
    else:
        logger.info(f"El usuario: {user.username}: {user.first_name} intento hacer sync")

def echo(update: Update, context):
    user = update.effective_user
    text = update.message.text
    if text.lower() == 'hi':
        update.message.reply_text(f"""Hi {user.first_name}!
                                          /\_/\  (
                                         ( ^.^ ) _)
                                           \\"/  (
                                          ( || )
                                       (__d b__)
           """)

    if text == 'bye':
        update.message.reply_text("a mimir! :3")


def downloader(update: Update, context):
    user = update.effective_user
    if not update.effective_chat.type == 'private':
        try:
            document = update.message.document
            file = context.bot.get_file(document).download()
            fileName = update.message.document.file_name
            fileExt = update.message.document.mime_type
            fileSize = update.message.document.file_size
            if fileExt == 'text/html':
                if fileSize < 1000000:
                    message = main(file, fileName)
                    verify = len(message)
                    if verify > 2:
                        update.message.reply_text(message[2])
                    else:
                        consulta2 = message[1][:-2] + ";"
                        fileName = fileName.split(".html")[0]
                        run_query(message)
                        update.message.reply_text(
                            f"select id into @lesson_id from lesson where name='{fileName}';\n" + consulta2 + "\n\nListo 👍 Los datos seran insertados. Espero entre 10 a 20 segundos")
                    remove('./' + file)
                    logger.info(f"El usuario: {user.username}: {user.first_name} inserto los id_tags")
                else:
                    update.message.reply_text('el archivo pesa mas de 1mb')
        except Exception as e:
            update.message.reply_text("Ha ocurrido un error. Puede que ya existan los id_tags")
            logger.error(e)
    else:
        logger.info(f"El usuario: {user.username}: {user.first_name} intento agregar id tags en el privado")



def main_start():
    """Run bot."""
    # Create the Updater and pass it your bot's token.
    updater = Updater(AUTH_CONFIG['API_KEY'], use_context=True)
    dispatcher = updater.dispatcher
    dispatcher.add_handler(CommandHandler('start', start))
    dispatcher.add_handler(CommandHandler('sync', sync))
    dispatcher.add_handler(MessageHandler(Filters.text, echo))
    dispatcher.add_handler(MessageHandler(Filters.document, downloader))

    # Start the Bot
    updater.start_polling()

    # Run the bot until the user presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT
    updater.idle()


if __name__ == '__main__':
    main_start()