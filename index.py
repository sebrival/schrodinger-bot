import re
import MySQLdb
from config import AUTH_CONFIG


def run_query(query): 
    datos = [AUTH_CONFIG['DB_HOST'], AUTH_CONFIG['DB_USER'],AUTH_CONFIG['DB_PASS'], AUTH_CONFIG['DB_NAME']]
    conn = MySQLdb.connect(*datos) # Conectar a la base de datos 
    cursor = conn.cursor()         # Crear un cursor 
    consulta1 = query[0]

    cursor.execute(consulta1)          # Ejecutar una consulta

    if consulta1.upper().startswith('SELECT'): 
        data = cursor.fetchall()   # Traer los resultados de un select
        cursor.close()  
        id = data[0][0]
         
        consulta2 = query[1][:-2] + ";"
        consulta2 = consulta2.replace('@lesson_id', f'{id}')
        # print(consulta2)
        cursor = conn.cursor()         # Crear un cursor 
        cursor.execute(consulta2) 
        conn.commit() 

        cursor.close()                 # Cerrar el cursor 
        conn.close() 


def openFile(file: str) -> str:
    """ Receives the name of the file 
        and returns the content.
    """
    with open(file,encoding="utf-8") as html:
        content = html.read()
    return content

def searchAll(content: str) -> str:
    """ Receives the content and using
        regular expressions find all the 'id_tag'.
    """
    patronIdTag = re.compile('id_tag\s*=\s*[\'"](.+?)[\'"]')
    idTags = patronIdTag.findall(content)

    return idTags

def queryMessage(idTags: list, fileName: str) -> str:
    """ Receives a list of id tags and the name of the file, 
        then the query is structured and checks if it contains duplicate id tags.
    """
    # message =  f"select id into @lesson_id from lesson where name='{fileName}';\n"
    message =  [f"select * from lesson where name='{fileName}';"]
    message.append('insert into activity(lesson_id, id_tag) VALUES\n')
    for idTag in idTags:
        message[1] = message[1] + f"(@lesson_id, '{idTag}'),\n"
        # message = (f" {message} insert into activity(lesson_id, id_tag) VALUES (@lesson_id, '{idTag}');  \n")

    # if len(set(idTags)) != len(idTags):
    #     # message = message +  "El archivo contiene id_tags duplicados"
    #     message.append("El archivo contiene id_tags duplicados")
    return  message

def checkId(idTags: list, fileName: str) -> list:
    """ Receives a list of id tags and the name of the file, 
        then checks the syntax of each id tag.
    """
    invalidId = []
    baseId = fileName + "_ex_"
    lenght = len(idTags)
    for index in range(lenght):
        if index == 0:
            if not verifyId(
                currentIdTag=idTags[index], 
                previousIdTag=idTags[index], 
                baseId=baseId
                ):
                invalidId.append(idTags[index])
        else:
            if not verifyId(
                currentIdTag=idTags[index], 
                previousIdTag=idTags[index-1], 
                baseId=baseId
                ):
                invalidId.append(idTags[index])

    return invalidId


def verifyId(currentIdTag: str, previousIdTag: str, baseId: str) -> bool:
    """ Receives the current tag id, the previous tag id, and the base tag id, 
        then checks if the tag id is correct.
    """
    try:
        #Toda esta parte se debe optimizar, pero por ahora funciona xd
        currentIdTagParts = currentIdTag.split('_ex_')
        previousIdTagParts = previousIdTag.split('_ex_')

        if (currentIdTagParts[0] + "_ex_" == baseId):
            if currentIdTag == previousIdTag:
                return True
            if(len(currentIdTagParts[1]) == 1):
                if(len(previousIdTagParts[1]) == 1):
                    return int(currentIdTagParts[1]) == int(previousIdTagParts[1]) + 1
                return int(currentIdTagParts[1]) == int(previousIdTagParts[1].split('_')[0]) + 1

            if (currentIdTagParts[1].split('_')[0] == previousIdTagParts[1].split('_')[0]):
                return int(currentIdTagParts[1].split('_')[1]) == int(previousIdTagParts[1].split('_')[1]) + 1
            
            return int(currentIdTagParts[1].split('_')[0]) == int(previousIdTagParts[1].split('_')[0]) + 1

        return False
    except:
        return False

def main(file: str, fileName : str) -> str:
    fileName =  fileName.split(".html")[0]
    content = openFile(file)
    idTags = searchAll(content)
    query = queryMessage(
        idTags=idTags, 
        fileName=fileName
        )
    invalidIdTag = checkId(
        idTags=idTags, 
        fileName=fileName
        )
        
    if invalidIdTag:
        # query = query + "El archivo contiene id_tags invalidos o desordenados \n"
        query.append("El archivo contiene id_tags invalidos o desordenados \n")
        for invalid in invalidIdTag:
            print(invalid)
            query[2] = query[2] + f"{invalid} \n"    
    return query

